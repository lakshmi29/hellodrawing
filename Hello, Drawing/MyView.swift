//
//  MyView.swift
//  Hello, Drawing
//
//  Created by Kalvakuri,Lakshmi Seshu on 4/9/19.
//  Copyright © 2019 northwest. All rights reserved.
//

import UIKit

class MyView: UIView {
    var points:[CGPoint] = []
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        points.append((touch?.location(in: self))!)
        print("Began: \(String(describing: touch?.location(in: self)))")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        points.append((touch?.location(in: self))!)
        print("Moved: \(String(describing: touch?.location(in: self)))")
        setNeedsDisplay()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        points.append((touch?.location(in: self))!)
        print("Ended: \(String(describing: touch?.location(in: self)))")
    }
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        if points.count == 0 {
            return
        }
        let path = UIBezierPath()
        path.move(to:points[0])
        for pt in points {
            path.addLine(to: pt)
        }
        path.stroke()
    }
    
    
//        path.move(to: CGPoint(x: 200.0, y:50.0))
//        path.addLine(to: CGPoint(x: 20.0, y:150.0))
//        path.addLine(to: CGPoint(x: 50.0, y: 30.0))
//        path.close()
//        UIColor.green.setStroke()
//        UIColor.blue.setFill()
//        path.lineWidth = 8.0
//        path.stroke()
//        path.fill()
    
//        let path2 = UIBezierPath()
//        path2.move(to:CGPoint(x:100.0,y:292.0))
//        path2.addArc(withCenter: CGPoint(x:50.0,y:50.0), radius:25.0, startAngle: 0.0, endAngle: CGFloat(Double.pi/2.0),clockwise: true)
//        path2.stroke()

        
        
}
